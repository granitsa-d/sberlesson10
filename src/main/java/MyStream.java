import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collector;

public class MyStream<T> {
    private final List<T> list;

    public MyStream(List<? extends T> list) {
        this.list = new ArrayList<>(list);
    }


    public static <T> MyStream<T> of(List<T> list) {
        return new MyStream<>(list);
    }

    public MyStream<T> filter(Predicate<? super T> predicate) {
        list.removeIf(predicate.negate());
        return this;
    }

    public <R> MyStream<R> map(Function<? super T, ? extends R> mapper) {
        List<R> result = new ArrayList<>();

        for (T t : list) {
            R r = mapper.apply(t);
            result.add(r);
        }
        return new MyStream<R>(result);
    }

    public MyStream<T> limit(long maxSize) {
        return new MyStream<T>(list.subList(0, (int) maxSize));
    }

    public MyStream<T> distinct() {
        HashSet set = new HashSet(list);
        list.clear();
        list.addAll(set);
        return new MyStream<T>(list);
    }

    public void forEach(Consumer<? super T> action) {
        for (T t : list) {
            action.accept(t);
        }
    }

    public List<T> toList() {
        return new ArrayList<T>(list);
    }

    public <K, V> Map<K, V> toMap(Function<? super T, ? extends K> keyMapper,
                                  Function<? super T, ? extends V> valueMapper) {
        Map<K, V> map = new HashMap<>();

        for (T t : list) {
            K k = keyMapper.apply(t);
            V v = valueMapper.apply(t);
            map.put(k, v);
        }
        return map;
    }

    public T findFirst() {
        forEach(Objects::nonNull);
        return list.get(0);
    }

    public int count(){
        forEach(Objects::isNull);
        return list.size();
    }
}
