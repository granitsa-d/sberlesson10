import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        Stream<Integer> stream = Stream.of(1, 3, 2, 5, 7, 6);
        stream
                .map(i -> i.toString() + "10")
                .limit(2)
                .distinct()
                .findFirst();

        System.out.println(stream);

        List<Integer> list = new LinkedList<>();
        list.add(1);
        list.add(1);
        list.add(10);
        list.add(2);
        list.add(45);
        list.add(3);
        list.add(5);
        list.add(1);

        List<String> list1 = new LinkedList<>();
        list1.add("qwe");
        list1.add("asd");

        MyStream<Integer> myStream = (MyStream<Integer>) MyStream.of(list);
        int a = myStream
                .filter(i -> i < 10)
                .map(i -> i + 10)
                .limit(3)
                .distinct()
                .count();


        System.out.println(a);
    }
}
